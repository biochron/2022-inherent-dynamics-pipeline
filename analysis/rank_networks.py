import json,os

def score_by_pattern_match_domain(results_domain):
    all_networks = []
    for net,counts in results_domain.items():
        scores = []
        # noise levels are in increasing order
        for k,noises in enumerate(counts):
            pattern_match_domain = noises[1]
            pg_size = noises[2]
            pattern_match_domain = results_domain[net][k][1]
            scores.append((0,pattern_match_domain / pg_size))
        all_networks.append((net,tuple(scores)))
    return sorted(all_networks, key=lambda x: x[1], reverse=True)


def score_by_pattern_match_stable_fc(results_fc):
    all_networks = []
    for net,counts in results_fc.items():
        scores = []
        # noise levels are in increasing order
        for k,noises in enumerate(counts):
            pattern_match_fc = noises[1]
            fc = noises[2]
            pg_size = noises[3]
            if fc:
                scores.append((pattern_match_fc/fc, fc/pg_size))
            else:
                scores.append((0,0))
        all_networks.append((net,tuple(scores)))
    return sorted(all_networks, key=lambda x: x[1], reverse=True)


def nonzero_pattern_match_domain(scores):
    nonzero = []
    for net,score in scores:
        if any([s[1] for s in score]):
            nonzero.append((net,score))
    return nonzero


def nonzero_pattern_match_fc(scores):
    nonzero = []
    for net,score in scores:
        if any([s[0] for s in score]):
            nonzero.append((net,score))
    return nonzero


def get_top_scores(all_networks,top_value):
    # top value is a 2-tuple, example: (pattern match / fc, fc / pg_size)
     return [s for s in all_networks if all(t[0] >= top_value[0] and t[1] >= top_value[1] for t in s[1]) ]


def get_top_scores_range(all_networks,top_value):
    # top value is a 2-tuple of lists with min and max values, example: ([min_pattern match / fc,max_pattern match / fc], [min_fc / pg_size, max_fc / pg_size] )
     return [s for s in all_networks if all(t[0] >= top_value[0][0] and t[0] <= top_value[0][1] and t[1] >= top_value[1][0] and t[1] <= top_value[1][1] for t in s[1]) ]



def matches2posets_stablefc(results_dirname,top_results):
    top_nets = [p[0] for p in top_results]
    nets_only = [set(top_nets)]
    numposets = 0
    for fname in os.listdir(results_dirname):
        if "fc" in fname and "all" not in fname:
            numposets += 1
            param_file_fc = os.path.join(results_dirname,fname)
            results_fc = json.load(open(param_file_fc))
            all_networks = score_by_pattern_match_stable_fc(results_fc)
            nonzero = nonzero_pattern_match_fc(all_networks)
            nets_only.append(set([t[0] for t in nonzero]))
    nets_intersected = set.intersection(*nets_only)
    return numposets, [p for p in top_results if p[0] in nets_intersected]


def matches2posets_domain(results_dirname,top_results):
    top_nets = [p[0] for p in top_results]
    nets_only = [set(top_nets)]
    numposets = 0
    for fname in os.listdir(results_dirname):
        if "domain" in fname and "all" not in fname:
            numposets += 1
            param_file_domain = os.path.join(results_dirname,fname)
            results_domain = json.load(open(param_file_domain))
            all_networks = score_by_pattern_match_domain(results_domain)
            nonzero = nonzero_pattern_match_domain(all_networks)
            nets_only.append(set([t[0] for t in nonzero]))
    nets_intersected = set.intersection(*nets_only)
    return numposets, [p for p in top_results if p[0] in nets_intersected]

