import json, os, sys
import rank_networks as rank
import top_lem
from statistics import median, mean
import dsgrn_utilities.graphtranslation as gt
import numpy as np


def performance_reporting(results_folder,pm_fc_value,fc_value,printout):
    if os.path.isdir(results_folder):
        results_fc = json.load(open(os.path.join(results_folder, "query_results_stablefc_all.json")))
    else:
        # allow a file to be entered instead of a folder
        results_fc = json.load(open(os.path.join(results_folder)))
    all_networks = rank.score_by_pattern_match_stable_fc(results_fc)
    nonzero = rank.nonzero_pattern_match_fc(all_networks)
    if isinstance(pm_fc_value,list):
        top_results = rank.get_top_scores_range(nonzero,(pm_fc_value, fc_value))
    else:
        top_results = rank.get_top_scores(nonzero,(pm_fc_value, fc_value))
    top = dict(top_results)
    if os.path.isdir(results_folder):
        num_posets,matches = rank.matches2posets_stablefc(results_folder,top_results)
    else:
        # allow a file to be entered instead of a folder
        num_posets = 1
        matches = top_results
    pms = dict(matches)
    best = set(top.keys()).intersection(set(pms.keys()))
    best_networks = {b: score for b, score in top.items() if b in best}
    if printout:
        print("\n")
        print("Total networks tested: {}".format(len(all_networks)))
        print("Number of networks with nonzero pattern match score: {}\n".format(len(nonzero)))
        if isinstance(pm_fc_value,list):
            if top_results:
                print("Number of networks with stable fc pattern matches in the range {}-{}% for at least one dataset\nplus stable fc in parameter graph in the range {}-{}% for all {} noise levels: {} networks".format(*[v * 100 for v in pm_fc_value], *[v * 100 for v in fc_value], len(top_results[0][1]), len(top_results)))
            else:
                print("Number of networks with stable fc pattern matches in the range {}-{}% for at least one dataset\nplus stable fc in parameter graph in the range {}-{}%: 0} networks".format(*[v * 100 for v in pm_fc_value], *[v * 100 for v in fc_value]))
        else:
            if top_results:
                print("Number of networks with >={}% stable fc pattern matches for at least one dataset\nplus >={}% stable fc in parameter graph for all {} noise levels: {} networks".format(pm_fc_value * 100, fc_value * 100, len(top_results[0][1]), len(top_results)))
            else:
                print("Number of networks with >={}% stable fc pattern matches for at least one dataset\nplus >={}% stable fc in parameter graph: 0 networks".format(pm_fc_value * 100, fc_value * 100))
        print("Plus matches for all {} posets in stable FCs: {} networks".format(num_posets,len(matches)))
    total_tested = len(all_networks)
    validated = len(nonzero)
    top_networks = len(best_networks)
    return total_tested, validated, top_networks, best_networks


def edges_reranking(best_networks,lem_score_file,gte,printout):
    best_nets = list(best_networks.keys())
    edges = edge_prevalence(best_nets)
    lem_dict = top_lem.get_LEM_ranking(lem_score_file)
    ranked_edges = []
    for e in edges:
        ranked_edges.append([e[0],e[1],lem_dict[e[0]]])
    ranked_edges = sorted(ranked_edges,key=lambda x : (x[1],1/x[2]),reverse=True)
    ranked_edges = [tuple(e+[k+1]) for k,e in enumerate(ranked_edges)]
    if printout:
        print("\n")
        print("Edge, prevalence score, LEM ranking, reranking in best networks:")
        for e,c,r,d in ranked_edges:
            print("{} & {:.2f}% & {} & {}".format(e,c*100,r,d))
    print("\n")
    print("Edge, prevalence score, LEM ranking, reranking in best networks for ground truth edges only:")
    for e,c,r,d in ranked_edges:
        if e in gte:
            print("{} & {:.2f}% & {} & {}".format(e,c*100,r,d))
    top_ten = [e[0] for e in ranked_edges[:10]]
    print("\n")
    print("Count of ground truth edges in top 10 DSGRN networks: {}".format(len(set(gte).intersection(top_ten))))
    return ranked_edges, lem_dict


def get_rank_change(ranked_edges, gte):
    tp_rank_change = {}
    for edge,_,lem,dsgrn in ranked_edges:
        if edge in gte:
            tp_rank_change[edge] = (lem, dsgrn, lem - dsgrn)
    total_tp_score = 0
    for e,score in tp_rank_change.items():
        total_tp_score += score[2]
    return tp_rank_change, total_tp_score


def read_inputs(param_dict,gte):
    seed_net = open(param_dict["seed_file"]).read()
    graph = gt.getGraphFromNetworkSpec(seed_net)
    seed_edges = ["{}={}({})".format(graph.vertex_label(t),graph.edge_label(s,t),graph.vertex_label(s)).replace("a(","tf_act(").replace("r(","tf_rep(") for s,t in graph.edges()]
    allowable_edges = seed_edges + [e.replace("a(","tf_act(").replace("r(","tf_rep(").strip() for e in open(param_dict["edge_file"]).readlines()]
    correct_seed = len(set(seed_edges).intersection(gte))
    incorrect_seed = len(seed_edges) - correct_seed
    gte_in_allowable = set(allowable_edges).intersection(gte)
    return allowable_edges,correct_seed,incorrect_seed,gte_in_allowable



def top_edges_stats(tp_rank_change, total_tp_score,ranked_edges,param_dict,lem_dict,gte,printout):
    # tp_rank_change is ground truth edge rank changes, (lem, dsgrn, lem-dsgrn)
    # total_tp_score is the sum of all positive and negative rank changes
    # ranked_edges is all top DSGRN edges
    rank_change = [x[2] for x in tp_rank_change.values()]
    lem_ranks = [x[0] for x in tp_rank_change.values()]
    dsgrn_ranks = [x[1] for x in tp_rank_change.values()]
    print("\n")
    print("True positive rank change score = {}".format(total_tp_score))
    if not rank_change:
        rank_change=[0]
    print("Range of rank changes = ({}, {})".format(min(rank_change),max(rank_change)))
    print("\n")
    if not lem_ranks:
        lem_ranks = [0]
    medlem = median(lem_ranks)
    meanlem = mean(lem_ranks)
    rangelem = (min(lem_ranks), max(lem_ranks))
    if not dsgrn_ranks:
        dsgrn_ranks = [0]
    meddsgrn = median(dsgrn_ranks)
    meandsgrn = mean(dsgrn_ranks)
    rangedsgrn = (min(dsgrn_ranks), max(dsgrn_ranks))
    top_dsgrn_edges = [e[0] for e in ranked_edges]
    gte_top_dsgrn = set(top_dsgrn_edges).intersection(gte)
    allowable_edges,correct_seed,incorrect_seed,gte_in_allowable = read_inputs(param_dict,gte)
    tp_dropped = set(gte_in_allowable).difference(gte_top_dsgrn)
    fp_dropped = len(allowable_edges) - len(top_dsgrn_edges) - len(tp_dropped)
    if printout:
        print("Mean LEM rank of ground truth in top DSGRN edges = {}".format(meanlem))
        print("Median LEM rank of ground truth in top DSGRN edges = {}".format(medlem))
        print("Mean DSGRN rank of ground truth in top DSGRN edges = {}".format(meandsgrn))
        print("Median DSGRN rank of ground truth in top DSGRN edges = {}".format(meddsgrn))
        print("Range of LEM ranks of ground truth in top DSGRN edges = {}".format(rangelem))
        print("Range of DSGRN ranks of ground truth in top DSGRN edges  = {}".format(rangedsgrn))
        print("\n")
        print("There are {}/{} ground truth edges in the list of {} DSGRN top edges".format(len(gte_top_dsgrn), len(gte), len(top_dsgrn_edges)))
        print("There are {} ground truth edge(s) in the seed and {} non-ground truth edge(s) in the seed.".format(correct_seed,incorrect_seed))
        print("Number of ground truth edges in allowable edges: {}".format(len(gte_in_allowable)))
        print("Number of edges kept in revised ranking list: {}/{}".format(len(top_dsgrn_edges),len(allowable_edges)))
        print("False positive(s) dropped from DSGRN top edges: {}".format(fp_dropped))
        print("True positive(s) dropped from DSGRN top edges: {}".format(len(tp_dropped)))
        for e in tp_dropped:
            print(e + ", LEM rank {}".format(lem_dict[e]))
        print("\n")
    return correct_seed, incorrect_seed, medlem, meddsgrn, len(tp_dropped), fp_dropped, len(gte_in_allowable), len(allowable_edges)


def get_gte_lem_scores(lem_dict,gte):
    gte_lem = [v for e,v in lem_dict.items() if e in gte]
    print("Count of ground truth edges in the top 10 LEM ranked edges: {}".format(sum([g<=10 for g in gte_lem])))
    print("Worst LEM rank of a ground truth edge over all edges: {}".format(max(gte_lem)))


def edge_prevalence(networks):
    N = len(networks)
    edges = {}
    for spec in networks:
        lines = spec.split("\n")
        while "" in lines:
            lines.remove("")
        nodes = [(line.split(":")[0].replace(" ",""),line.split(":")[1].replace(" ","")) for line in lines]
        for node, inputs in nodes:
            inedges = inputs.replace("(", " ").replace(")", " ").replace("+"," ").split()
            fulledges = [node+"=tf_act("+i+")" if "~" not in i else node+"=tf_rep("+i[1:]+")" for i in inedges]
            for e in fulledges:
                if e not in edges:
                    edges[e] = 1
                else:
                    edges[e] += 1
    edges = {e : c/N for e,c in edges.items()}
    return sorted(edges.items(),key=lambda x : (x[1],x[0]),reverse=True)


def compute_median_rank(ranked_edges,lem_dict,param_dict):
    nodes = param_dict["avg_rank_nodes"]
    seed_net = open(param_dict["seed_file"]).read()
    graph = gt.getGraphFromNetworkSpec(seed_net)
    seed_edges = ["{}={}({})".format(graph.vertex_label(t),graph.edge_label(s,t),graph.vertex_label(s)).replace("a(","tf_act(").replace("r(","tf_rep(") for s,t in graph.edges()]    
    allowable_edges = seed_edges + [e.replace("a(","tf_act(").replace("r(","tf_rep(").strip() for e in open(param_dict["edge_file"]).readlines()]
    N = len(allowable_edges)
    extra_edges = [(e,0,None,N) for e in allowable_edges if e not in [f[0] for f in ranked_edges]]
    ranked_edges = ranked_edges + extra_edges
    lem_ranked_edges = [(e,r) for e,r in lem_dict.items() if e in allowable_edges ]
    N = len(ranked_edges)
    results = []
    for node in nodes:
        reranked_node_ranks = [e[-1] for e in ranked_edges if node in e[0]] 
        lem_node_ranks = [r for e,r in lem_dict.items() if node in e and e in allowable_edges ]
        if reranked_node_ranks:
            avg_rerank = median(reranked_node_ranks)
        else:
            avg_rerank = np.nan
        if lem_node_ranks:
            avg_lem = median(lem_node_ranks)
        else:
            avg_lem = np.nan
        results.append((avg_rerank,avg_lem,node))
    results.sort()
    print("Node, median rerank, median lem rank\n")
    results_dict = {}
    for r in results:
        print("{} \t& {:.1f} \t& {:.1f}".format(r[2],r[0],r[1]))
        results_dict[r[2]] = (r[0],r[1])
    ranked_edges = [(t[0],(t[-1],t[1])) for t in ranked_edges]
    dsgrn = dict(ranked_edges)
    lem = dict(lem_ranked_edges)
    edges_dict = {e : (d,lem[e]) for e,d in dsgrn.items()}
    return results_dict, edges_dict


def main(param_dict,drop_node=None):
    total_tested, validated, top_networks, best_networks = performance_reporting(param_dict["results_folder"],param_dict["pm_fc_value"],param_dict["fc_value"],True)
    summary = {"networks_tested" : total_tested, "networks_validated" : validated, "top_networks" : top_networks}
    gte =  param_dict["ground_truth_edges"]
    if drop_node:
        gte = [e for e in gte if drop_node not in e]
    printout = param_dict["show_entire_edge_list"] if "show_entire_edge_list" in param_dict else False
    ranked_edges, lem_dict = edges_reranking(best_networks, param_dict["lem_score_file"],gte,printout)
    summary["lem_edges_analyzed"] = len(lem_dict)
    tp_rank_change, total_tp_score = get_rank_change(ranked_edges, gte)
    get_gte_lem_scores(lem_dict,gte)
    correct_seed, incorrect_seed, medlem, meddsgrn, tp_dropped, fp_dropped, tp_in_allowable, allowable = top_edges_stats(tp_rank_change, total_tp_score,ranked_edges,param_dict,lem_dict,gte,True)
    summary.update({"true_pos_in_seed" : correct_seed, "false_pos_in_seed" : incorrect_seed, "median_rerank_true_pos":meddsgrn, "median_lem_rank_true_pos_in_rerank" : medlem,"tp_dropped" : tp_dropped, "fp_dropped":fp_dropped, "true_pos_in_allowable":tp_in_allowable, "allowable_edges":allowable})
    if "avg_rank_nodes" in param_dict:
        results, edges_dict = compute_median_rank(ranked_edges,lem_dict,param_dict)
        summary["edge_ranks"] = edges_dict
        summary["node_median_ranks"] = results
    print("\n")
    return summary



if __name__ == "__main__":
    # arg 1 is analysis params file
    # arg 2 is node to drop
    param_dict = json.load(open(sys.argv[1]))
    if len(sys.argv) >2:
        main(param_dict,sys.argv[2])
    else:
        main(param_dict)




