def count_all_edges(networks):
    N = len(networks)
    edges = {}
    for spec in networks:
        lines = spec.split("\n")
        while "" in lines:
            lines.remove("")
        nodes = [(line.split(":")[0].replace(" ",""),line.split(":")[1].replace(" ","")) for line in lines]
        for node, inputs in nodes:
            inedges = inputs.replace("(", " ").replace(")", " ").replace("+"," ").split()
            fulledges = [node+"=tf_act("+i+")" if "~" not in i else node+"=tf_rep("+i[1:]+")" for i in inedges]
            for e in fulledges:
                if e not in edges:
                    edges[e] = 1
                else:
                    edges[e] += 1
    edges = {e : c/N for e,c in edges.items()}
    return sorted(edges.items(),key=lambda x : (x[1],x[0]),reverse=True)


