import numpy as np
from main_analysis import main as analyze_computation
import sys,json,ast


def compile_summaries(list_of_summaries):
    master_summary = {}
    for summary in list_of_summaries:
        for key,val in summary.items():
            if not isinstance(val,dict):
                if key not in master_summary:
                    master_summary[key] = [val]
                else:
                    master_summary[key].append(val)
            else:
                # manage the node dictionary of median ranks per node and edge rank dict
                for nodekey,nodeval in val.items():
                    if nodekey+"_lem" not in master_summary:
                        if "(" in nodekey:
                            master_summary[nodekey+"_lem"] = [nodeval[1]]
                            master_summary[nodekey+"_rerank"] = [nodeval[0][0]]
                            master_summary[nodekey+"_rerank_prevalence"] = [nodeval[0][1]]  
                        else:
                            master_summary[nodekey+"_lem"] = [nodeval[1]]
                            master_summary[nodekey+"_rerank"] = [nodeval[0]]
                    else:
                        if "(" in nodekey:
                            master_summary[nodekey+"_lem"].extend([nodeval[1]])
                            master_summary[nodekey+"_rerank"].extend([nodeval[0][0]])
                            master_summary[nodekey+"_rerank_prevalence"].extend([nodeval[0][1]])  
                        else:
                            master_summary[nodekey+"_lem"].extend([nodeval[1]])
                            master_summary[nodekey+"_rerank"].extend([nodeval[0]])

    return master_summary


def get_averages(master_summary):
    averages = {}
    for key,val_list in master_summary.items():
        averages[key] = (np.mean(val_list),np.std(val_list))
    return averages
        

def get_medians(edge_dict):
    medians = {}
    for key,val_list in edge_dict.items():
        medians[key] = (np.median(val_list),)
    return medians


def sort_lem_dsgrn_output(list_of_keys,stats_vals,averages):
    lem_avgs = []
    rerank_avgs = []
    prevalence_avgs = []
    for key in list_of_keys:
        lem_avgs.append((key,stats_vals[key+"_lem"]))
        rerank_avgs.append((key,*stats_vals[key+"_rerank"]))
        if "(" in key:
            prevalence_avgs.append((key,averages[key+"_rerank_prevalence"]))
    lem_ranks = dict(lem_avgs)
    if prevalence_avgs:
        prevalence = dict(prevalence_avgs)
        rerank_avgs = [list(r)+[*prevalence[r[0]]] for r in rerank_avgs]
        rank_avgs = [r + [lem_ranks[r[0]][0]] for r in rerank_avgs]
        rerank_ranks = sorted(rank_avgs, key = lambda x: (1/x[1],x[2],1/x[4]),reverse=True)
    else:
        rank_avgs = [list(r) + list(lem_ranks[r[0]]) for r in rerank_avgs]
        rerank_ranks = sorted(rank_avgs, key = lambda x: (x[1],x[2]))
    return rerank_ranks

        
def get_summaries(list_of_param_files,drop_node):
    list_of_summaries = []
    for pfile in list_of_param_files:
        param_dict = json.load(open(pfile))
        summary = analyze_computation(param_dict,drop_node)
        list_of_summaries.append(summary)
    return list_of_summaries


def print_stats(list_of_param_files,drop_node=None):
    # make dict of lists of values
    list_of_summaries = get_summaries(list_of_param_files,drop_node)
    master_summary = compile_summaries(list_of_summaries)

    # take averages of lists
    averages = get_averages(master_summary)


    #handle edge dictionary -- need intersection of edges in allowable edges
    edge_dict = {}
    intersection_edges = [s["edge_ranks"].keys() for s in list_of_summaries]
    intersection_edges = set.intersection(*map(set,intersection_edges))
    for key in master_summary.keys():
        if "(" in key and "_rerank" in key:
            edge = key.split(")")[0] + ")"
            if edge in intersection_edges:
                edge_dict[key] = master_summary[key]
                edge_dict[key+"_prevalence"] = master_summary[key]
                edge_dict[edge+"_lem"] = master_summary[edge+"_lem"]
    edge_medians = get_medians(edge_dict)

    # sort edge medians
    edge_ranks = sort_lem_dsgrn_output(intersection_edges,edge_medians,averages)
    
    #sort node averages
    nodes = list_of_summaries[0]["node_median_ranks"].keys()
    node_ranks = sort_lem_dsgrn_output(nodes,averages,averages)


    # print output
    print("\n\n%%%%%%%%%%%%%%%\nMaster Summary\n%%%%%%%%%%%%%%%\n\n")

    print("Edge Finding\n")
    print("Total edges analyzed & True positives in seed & False positives in seed & True pos in allowable ")
    print("{} & ${:.2f} \pm {:.2f}$ & ${:.2f} \pm {:.2f}$ & ${:.2f} \pm {:.2f}$".format(int(averages["lem_edges_analyzed"][0]),averages["true_pos_in_seed"][0],averages["true_pos_in_seed"][1],averages["false_pos_in_seed"][0],averages["false_pos_in_seed"][1],averages["true_pos_in_allowable"][0],averages["true_pos_in_allowable"][1]))

    print("\n\nNetwork Finding\n")
    print("Validated/Total & Top networks & True pos dropped & False pos dropped")
    print("${} \pm {}$ / {} & ${} \pm {}$  & ${:.2f} \pm {:.2f}$  & ${:.2f} \pm {:.2f}$".format(int(averages["networks_validated"][0]),int(averages["networks_validated"][1]),int(averages["networks_tested"][0]),int(averages["top_networks"][0]),int(averages["top_networks"][1]),averages["tp_dropped"][0],averages["tp_dropped"][1],averages["fp_dropped"][0],averages["fp_dropped"][1]))

    print("\n\nMedian true positive ranks")
    print("LEM & Rerank\n")
    print("${:.2f} \pm {:.2f}$ & ${:.2f} \pm {:.2f}$".format(averages["median_lem_rank_true_pos_in_rerank"][0],averages["median_lem_rank_true_pos_in_rerank"][1],averages["median_rerank_true_pos"][0],averages["median_rerank_true_pos"][1]))

    print("\n\nMedian node ranks")
    print("Node &  LEM & Rerank")
    for node,rerank_avg,rerank_std, lem_avg,lem_std in node_ranks:
        print("{} & ${:.2f} \pm {:.2f}$ & ${:.2f} \pm {:.2f}$ \\\\".format(node,lem_avg,lem_std,rerank_avg,rerank_std))

    results_dict = {"file_names" : list_of_param_files, "node_ranks" : node_ranks, "doc_string" : "node_ranks is a list of lists, where each sublist has five elements in the order: node name, average dsgrn rank, std dev dsgrn rank, average lem rank, std dev lem rank"}
    json.dump(results_dict,open("node_rankings.json","w"))

    
    print("\n\nMedian edge ranks and average prevalence")
    print("Edge & Prevalence & Rerank & LEM ")
    for edge,rerank_median, prev_avg, prev_std,lem_med in edge_ranks:
        print("{} & ${:.1f}\% \pm {:.1f}\%$ & ${}$ & ${}$ \\\\".format(edge,prev_avg*100,prev_std*100,int(rerank_median),int(lem_med)))

    master_summary["file_names"]=list_of_param_files
    json.dump(master_summary,open("master_summary.json","w"))




if __name__ == "__main__":
    # arg 1 is list of analysis params files
    # optional arg 2 is node to drop
    if len(sys.argv) >2:
        print_stats(ast.literal_eval(sys.argv[1]),sys.argv[2])
    else:
        print_stats(ast.literal_eval(sys.argv[1]))
