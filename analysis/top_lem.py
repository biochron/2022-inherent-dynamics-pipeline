import pandas as pd


def get_LEM_ranking(LEM_file):
    df = pd.read_csv(LEM_file,sep="\t",comment="#")
    models = df["model"].values
    lem_dict = { m : i+1 for i,m in enumerate(models)}
    return lem_dict


def get_ranked_lem_edges(lem_file):
    df = pd.read_csv(lem_file,sep="\t",comment="#")
    lem_ranks = [x.strip() for x in df["model"].values]
    return lem_ranks


def get_edges_in_top(lem_ranks,edges,top_num):
    return len(set(lem_ranks[:top_num]).intersection(edges))


def get_worst_rank(lem_ranks,edges):
    vals = list(enumerate(lem_ranks))
    vals.reverse()
    for k,l in vals:
        if l in edges:
            return k+1


