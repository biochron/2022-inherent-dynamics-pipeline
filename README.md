## Overview

Scripts for generating the figures in the paper "Experimental Guidance for Discovering Genetic Networks through Iterative Hypothesis Reduction on Time Series", Cummins et al. (2022), DOI TBD.

## Running the Inherent Dynamics Pipeline

Install the `inherent_dynamics_pipeline` package available here: https://gitlab.com/biochron/inherent_dynamics_pipeline.

The following are commandline instructions (Linux or Mac) for generating similar results to those in the manuscript "Experimental Guidance for Discovering Genetic Networks through Iterative Hypothesis Reduction on Time Series". The results will not be identical due to the stochastic nature of the software.

To do a stochastic run of the Inherent Dynamics Pipeline, first change directories into the `inherent_dynamics_pipeline` repository, wherever it is installed on your machine. Be sure to activate the conda environment.

```bash
$ cd inherent_dynamics_pipeline
$ source activate dat2net
```

Then run the `inherent_dynamics_pipeline` command on the appropriate configuration file. Assuming that `inherent_dynamics_pipeline` and `2022-Inherent-Dynamics-Pipeline` are in the same parent folder, an example would be

```bash
$ python src/dat2net.py ../2022-Inherent-Dynamics-Pipeline/yeast_data/simulations202110_Orlando_2/annot/config.txt
```

This will do a stochastic run for the _S. cerevisiae_ data with only substantiated nodes and nontrivial annotations. You may want to change the `output_dir` variable to a different results folder, although if you do not, no results will be overwritten. There will be a folder with the date and time of the run in the `output_dir` listed in `config.txt`. There are similar configuration files for the scenario with only substantiated nodes and no annotations (`no_annot`), and both substantiated and unsubstantiated nodes with or without nontrivial annotations (`extra_nodes_annot` and `extra_nodes_no_annot`, respectively) nested in the `2022-Inherent-Dynamics-Pipeline/yeast_data/simulations202110_Orlando_2` folder. There are also configuration files in `2022-Inherent-Dynamics-Pipeline/syn_net/syn_net_6D_simulations_202110_with_G` in the subfolders `p270` (Fig. 2 (b)), `p779` (Fig. 2 (c)), and `p1945` (Fig. 2 (d)).

## Analysis

Once the results are generated, you can run the analysis code. The analysis code requires configuration files as well, which are in the `analysis_configs` subfolders of `syn_net` and `yeast_data`. To analyze your own stochastic run, change the file names as appropriate. The file names are stereotyped and it should be clear what needs to change. Then run
```bash
$ cd analysis_configs
$ python ../../../analysis/average_over_simulations.py "['config1', 'config2', ..., 'configN']"
```
where the argument is a list of analysis configuration files.
The quotes are important.

To rerun the analysis for the stochastic runs generated for the manuscript, do
```bash
$ cd analysis_configs
$ . do_all.sh
```

## Visualization

To generate the figures in the manuscript, there are several Jupyter notebooks. The notebook 
```
2022-Inherent-Dynamics-Pipeline/syn_net/creation_of_datasets_6D/syn_net_initial_runs.ipynb
```
plots the synthetic data (see the `README` in that folder for additional information on the creation of the datasets).

The following command generates the well and poorly ranked LEM edges histograms:
```bash
$ cd 2022-Inherent-Dynamics-Pipeline/syn_net/syn_net_6D_simulations_202110_with_G/lem_poor_edges_vs_good
$ python lem_poor_gen_figs.py
```

The notebook 
```
2022-Inherent-Dynamics-Pipeline/analysis/edge_and_node_ranking_figures.ipynb
```
produces the results figures.

Lastly, the notebook
```
2022-Inherent-Dynamics-Pipeline/yeast_data/time_series/plot_data.ipynb
```
plots the _S. cerevisiae_ microarray data for the genes in this study.
