import pandas as pd
import sys, os

def transpose_file(data_tsv):
    comments = open(data_tsv).readlines()[:2]
    base,ext = os.path.splitext(os.path.basename(data_tsv))
    new_fname = base+"_transposed"+ext
    new_full_fname = os.path.join(os.path.dirname(data_tsv),new_fname)
    df = pd.read_csv(data_tsv,sep="\t",comment="#")
    df = df.set_index("time").transpose().rename_axis("time")
    with open(new_full_fname,"w") as f:
        for comment in comments:
            f.write(comment)
        df.to_csv(f,sep="\t",index=True)


def transpose_file_old(data_tsv):
    base,ext = os.path.splitext(os.path.basename(data_tsv))
    new_fname = base+"_transposed"+ext
    new_full_fname = os.path.join(os.path.dirname(data_tsv),new_fname)
    df = pd.read_csv(data_tsv,sep="\t",comment="#")
    df = df.set_index("time").transpose().rename_axis("gene")
    df.to_csv(new_full_fname,sep="\t",index=True)

if __name__ == "__main__":
    transpose_file_old(sys.argv[1])