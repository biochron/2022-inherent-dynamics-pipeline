from min_interval_posets import poset_distance as podist
import json, os, random, time, sys
from collections import OrderedDict


def calc_dist(poset1,poset2):
    G = podist.poset_to_nx_graph(poset1)
    H = podist.poset_to_nx_graph(poset2)
    return podist.dag_distance(G,H)


def read_file(dirname,fname):
    results = json.load(open(os.path.join(dirname,fname)))
    return results["Poset at eps = 0.0"]


def calc_dist_for_one(start_poset,dirname,fnames):
    dists = {}
    for k,f in enumerate(fnames):
        comp_poset = read_file(dirname,f)
        d = calc_dist(start_poset,comp_poset)
        if d > 0:
            dists[f] = d
        if not k%50 and k>0:
            print("{} of {} poset distances calculated.".format(k,len(fnames)))
            sys.stdout.flush()
    return dict2sortedlist(dists)


def calc_random(start,dirname,fnames):
    start_poset = read_file(dirname,start)
    start_dists = calc_dist_for_one(start_poset,dirname,fnames)
    return start_dists


def dict2sortedlist(d, reverse=True):
    return sorted(list(d.items()), key=lambda kv: kv[1], reverse=reverse)


def list2ordereddict(l):
    return OrderedDict(l)


def choose_posets(dirname):
    fn = list(os.listdir(dirname))
    fnames = [f for f in fn if f.endswith(".json")]
    dists = [None]*4
    names = [None]*4
    start_time = time.time()
    print("Calculating 1 of 4")
    names[0] = random.choice(fnames)
    fnames.remove(names[0])
    dists[0] = calc_random(names[0],dirname,fnames)
    json.dump({names[0]:dists[0]},open("summary_choices_0.json","w"))
    dists[0] = list2ordereddict(dists[0])
    fnames = sorted(list(sorted(dists[0].keys())))
    print("Time passed for calculation {} is {}".format(1, time.time()-start_time))
    sys.stdout.flush()
    start_time = time.time()
    num_samps=4
    for i in range(1,num_samps):
        print("Calculating {} of 4".format(i+1))
        sys.stdout.flush()
        scores = {f : sum(dists[j][f] for j in range(0,i)) for f in fnames}
        r = dict2sortedlist(scores)
        names[i] = r[0][0]
        fnames.remove(names[i])
        pos = read_file(dirname,names[i])
        dists[i] = calc_dist_for_one(pos,dirname,fnames)
        json.dump({names[i]:dists[i]},open("summary_choices_{}.json".format(i),"w"))
        dists[i] = list2ordereddict(dists[i])
        fnames = sorted(list(sorted(dists[i].keys())))
        print("Time passed for calculation {} is {}".format(i+1,time.time()-start_time))
        start_time = time.time()
        sys.stdout.flush()
    print("\n")
    print(names)
    for i in range(4):
        d = dict2sortedlist(dists[i])
        print("Range for {} = {}.".format(i,(d[-1][1],d[0][1])))
    print("\n")
    for k,d in enumerate(dists[:-1]):
        print([d[n] for n in names[k+1:]])


def hack_partial_results():
    sum0 = json.load(open("summary_choices_0.json"))
    name0 = list(sum0.keys())[0]
    print("Range = [{}, {}]".format(sum0[name0][-1][1],sum0[name0][0][1]))
    set0 = sum0[name0][:50]
    sum1 = json.load(open("summary_choices_1.json"))
    name1 = list(sum1.keys())[0]
    print("Range = [{}, {}]".format(sum1[name1][-1][1],sum1[name1][0][1]))
    set1 = sum1[name1][:50]
    candidates = list(set([x[0] for x in set0]).intersection(set([x[0] for x in set1])))
    d0 = dict(set0)
    d1 = dict(set1)
    for c in candidates:
        print((c,d0[c],d1[c]))
    best = 'syn_net_6D_20191104_a_hillmodel_p1945.json'
    names = [best,name0,name1]
    print(names)
    print("Distances are {}".format([d0[name1],d0[best],d1[best]]))

if __name__ == "__main__":
    dirname = os.path.expanduser("~/Desktop/syn_net/syn_net_6D_20191104/")
    # choose_posets(dirname)
    hack_partial_results()







