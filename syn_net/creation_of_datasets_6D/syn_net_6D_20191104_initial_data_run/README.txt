The networks beginning `syn_net_6D_20191104` describe strongly-connected networks over a fixed node list of size 6 (A, B, C, D, E, F) and contain between 8 and 10 edges. syn_net_6D_20191104_a contains syn_net_6D_20191104_b as a subgraph, which contains syn_net_6D_20191104_c as a subgraph. 

In particular the edge `F activates E` has been removed from the network syn_net_6D_20191104_a to form syn_net_6D_20191104_b, and the edge and the edge `E represses D` has been removed create syn_net_6D_20191104_b to create syn_net_6D_20191104_c.

Each network exhibits 100% full cycles across their respective essential parameter spaces. syn_net_6D_20191104_a has 2016 parameters, syn_net_6D_20191104_b has 144, and syn_net_6D_20191104_c has 36.
