To generate a simulated dataset for a random parameter sample for the network in `syn_net_6D_20191104_a.txt`, alter the DSGRN parameter in `syn_net_traces.ipynb` (if desired; see `param_ind` in the third code box right beneath the network picture) and run it. A `data` file with the time series and a `hillmodel` file with the parameters will be created. See the manuscript "Experimental Guidance for Discovering Genetic Networks through
Iterative Hypothesis Reduction on Time Series" for information on the association of parameter names with Hill model equations.

To plot the synthetic data figures in the manuscript, use `syn_net_initial_runs.ipynb`. 

The folder `syn_net_6D_20191104_initial_data_run` contains a parameter sample for every DSGRN parameter for the network `syn_net_6D_20191104_a.txt`. These include the DSGRN parameters `270`, `779`, and `1945` used in the manuscript for Figure 2 b/f, c/g, and d/h, respectively.

