import matplotlib.pyplot as plt
import numpy as np
import json

def gen_fig(fname,edge_type):    
    results = json.load(open(fname))
    fcs = []
    pms = []
    for _,scores in results.items():
        s = scores[0]
        pms.append(0 if s[2] == 0 else s[1]/s[2])
        fcs.append(s[2]/s[3])
    fig,axs = plt.subplots(1,2,sharey=True,sharex=True)
    axs[0].hist(fcs,edgecolor='black',)
    axs[0].set_xlabel("Oscillation score",fontsize=14)
    axs[0].set_xlim([0,1])
    axs[1].hist(pms,edgecolor='black')
    axs[1].set_xlabel("Pattern match score",fontsize=14)
    axs[1].set_xlim([0,1])
    fig.suptitle("{}-ranked LEM edges".format(edge_type),fontsize=14)
    plt.tight_layout()
    plt.savefig("{}_ranked.pdf".format(edge_type),format="pdf")


if __name__ == "__main__":
    good_fname = "../p270/results1/network_queries_20211014210310/dsgrn_net_query_results20211014210310/queries20211014210310/query_results_stablefc_syn_net_6D_20191104_a_data_p270_transposed_with_G.json"
    gen_fig(good_fname,"Top")
    
    bad_fname = "poor_results/network_queries_20220307110240/dsgrn_net_query_results20220307110240/queries20220307110240/query_results_stablefc_syn_net_6D_20191104_a_data_p270_transposed_with_G.json"
    gen_fig(bad_fname,"Bottom")
    