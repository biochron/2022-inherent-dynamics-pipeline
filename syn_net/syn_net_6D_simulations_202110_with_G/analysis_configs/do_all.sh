#!/usr/bin/bash

python ../../../analysis/average_over_simulations.py "['analysis_params_p270_1.json','analysis_params_p270_2.json','analysis_params_p270_3.json','analysis_params_p270_4.json','analysis_params_p270_5.json']"

mv master_summary.json master_summary_Fig_1b.json

python ../../../analysis/average_over_simulations.py "['analysis_params_p779_1.json','analysis_params_p779_2.json','analysis_params_p779_3.json','analysis_params_p779_4.json','analysis_params_p779_5.json']"

mv master_summary.json master_summary_Fig_1c.json

python ../../../analysis/average_over_simulations.py "['analysis_params_p1945_1.json','analysis_params_p1945_2.json','analysis_params_p1945_3.json','analysis_params_p1945_4.json','analysis_params_p1945_5.json']"

mv master_summary.json master_summary_Fig_1d.json

