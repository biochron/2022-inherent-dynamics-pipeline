gene_list_file = ../pipeline/src/syn_net/syn_net_6D_simulations_202110_with_G/gene_list.txt
loss = euc_loss
data_files = ../pipeline/src/syn_net/time_series_with_G/syn_net_6D_20191104_a_data_p270_transposed_with_G.tsv,
verbose = True
num_proc = 64
annotation_file = ../pipeline/src/syn_net/syn_net_6D_simulations_202110_with_G/synnet6_annot_no_constraints.tsv
param_bounds = tf_param_bounds
prior = uniform_prior
normalize = True
inv_temp = 1
seed = 1634260999
output_dir = ../pipeline/src/syn_net/syn_net_6D_simulations_202110_with_G/p270/results2/edge_finding_20211014212318
config_file = ../pipeline/src/syn_net/syn_net_6D_simulations_202110_with_G/p270/results2/edge_finding_20211014212318/lempy_20211014212318_config.txt
[minimizer_params]
    niter = 10
    T = 1
    stepsize = 0.5
    interval = 10
    disp = False
    seed = 0
    accept_test = <function bounded_accept_test at 0x7fb1872fd710>
    take_step = <utilities.bounded_take_step object at 0x7fb186dac6d0>
[targets]
    A = ""
    B = ""
    C = ""
    D = ""
    E = ""
    F = ""
    G = ""
[regulators]
    A = tf_act, tf_rep
    B = tf_act, tf_rep
    C = tf_act, tf_rep
    D = tf_act, tf_rep
    E = tf_act, tf_rep
    F = tf_act, tf_rep
    G = tf_act, tf_rep
