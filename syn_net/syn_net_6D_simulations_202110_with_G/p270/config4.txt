data_file =  ../pipeline/src/syn_net/time_series_with_G/syn_net_6D_20191104_a_data_p270_transposed_with_G.tsv,

annotation_file = ../pipeline/src/syn_net/syn_net_6D_simulations_202110_with_G/synnet6_annot_no_constraints.tsv

output_dir = ../pipeline/src/syn_net/syn_net_6D_simulations_202110_with_G/p270/results4/

num_proc = 64
verbose = True
IDVconnection = True


[lempy_arguments]
gene_list_file = ../pipeline/src/syn_net/syn_net_6D_simulations_202110_with_G/gene_list.txt
loss = euc_loss

	[[minimizer_params]]
	niter = 10
	T = 1
	stepsize = .5
	interval = 10
	disp = False
	seed = 0


[netgen_arguments]
edge_score_column = pld
num_edges_for_list = 40
seed_threshold = 0.98


[netper_arguments]
range_operations = 2, 10
numneighbors = 2000
maxparams = 3000
time_to_wait = 1200
#random_seed = 0

[[probabilities]]
	addNode = 0.1
	addEdge = 0.9
	removeNode = 0.0
	removeEdge = 0.0

[[filters]]
    [[[is_strongly_connected]]]


[netquery_arguments]
stablefc = True
domain = False
count = True
epsilons = 0.00,
