data_file = ../pipeline/src/yeast_data/time_series/orlando2008_WildType_r1_trunc.tsv, ../pipeline/src/yeast_data/time_series/orlando2008_WildType_r2_trunc.tsv

annotation_file = ../pipeline/src/yeast_data/simulations202110_Orlando_2/yeast_no_annot_extra_nodes.tsv

output_dir = ../pipeline/src/yeast_data/simulations202110_Orlando_2/extra_nodes_no_annot/results5/

num_proc = 64
verbose = True
IDVconnection = True


[lempy_arguments]
gene_list_file = ../pipeline/src/yeast_data/simulations202110_Orlando_2/yeast_gene_list_extra_nodes.txt
loss = euc_loss

	[[minimizer_params]]
	niter = 10
	T = 1
	stepsize = .5
	interval = 10
	disp = False


[netgen_arguments]
edge_score_column = pld
num_edges_for_list = 75
seed_threshold = 0.98


[netper_arguments]
range_operations = 2, 10
numneighbors = 4000
maxparams = 2000
time_to_wait = 3600

[[probabilities]]
	addNode = 0.1
	addEdge = 0.6
	removeNode = 0.1
	removeEdge = 0.2

[[filters]]
    [[[is_strongly_connected]]]

[netquery_arguments]
stablefc = True
domain = False
count = True
epsilons = 0.05,

