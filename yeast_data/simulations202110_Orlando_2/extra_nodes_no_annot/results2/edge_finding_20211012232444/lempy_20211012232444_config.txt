gene_list_file = ../pipeline/src/yeast_data/simulations202110_Orlando_2/yeast_gene_list_extra_nodes.txt
loss = euc_loss
data_files = ../pipeline/src/yeast_data/time_series/orlando2008_WildType_r1_trunc.tsv, ../pipeline/src/yeast_data/time_series/orlando2008_WildType_r2_trunc.tsv
verbose = True
num_proc = 64
annotation_file = ../pipeline/src/yeast_data/simulations202110_Orlando_2/yeast_no_annot_extra_nodes.tsv
param_bounds = tf_param_bounds
prior = uniform_prior
normalize = True
inv_temp = 1
seed = 1634095485
output_dir = ../pipeline/src/yeast_data/simulations202110_Orlando_2/extra_nodes_no_annot/results2/edge_finding_20211012232444
config_file = ../pipeline/src/yeast_data/simulations202110_Orlando_2/extra_nodes_no_annot/results2/edge_finding_20211012232444/lempy_20211012232444_config.txt
[minimizer_params]
    niter = 10
    T = 1
    stepsize = 0.5
    interval = 10
    disp = False
    seed = 1634095485
    accept_test = <function bounded_accept_test at 0x7fce352e2830>
    take_step = <utilities.bounded_take_step object at 0x7fce34d94750>
[targets]
    SWI4 = ""
    HCM1 = ""
    NDD1 = ""
    SWI5 = ""
    NRM1 = ""
    CLN3 = ""
    WHI5 = ""
    YOX1 = ""
    ASH1 = ""
    RIF1 = ""
    EDS1 = ""
[regulators]
    SWI4 = tf_act, tf_rep
    HCM1 = tf_act, tf_rep
    NDD1 = tf_act, tf_rep
    SWI5 = tf_act, tf_rep
    NRM1 = tf_act, tf_rep
    CLN3 = tf_act, tf_rep
    WHI5 = tf_act, tf_rep
    YOX1 = tf_act, tf_rep
    ASH1 = tf_act, tf_rep
    RIF1 = tf_act, tf_rep
    EDS1 = tf_act, tf_rep
