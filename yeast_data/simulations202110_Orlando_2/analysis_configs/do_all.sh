#!/usr/bin/bash

python ../../../analysis/average_over_simulations.py "['analysis_params_annot_1.json','analysis_params_annot_2.json','analysis_params_annot_3.json','analysis_params_annot_4.json','analysis_params_annot_5.json']"

mv master_summary.json master_summary_annot.json

python ../../../analysis/average_over_simulations.py "['analysis_params_no_annot_1.json','analysis_params_no_annot_2.json','analysis_params_no_annot_3.json','analysis_params_no_annot_4.json','analysis_params_no_annot_5.json']"

mv master_summary.json master_summary_no_annot.json

python ../../../analysis/average_over_simulations.py "['analysis_params_extra_nodes_annot_1.json','analysis_params_extra_nodes_annot_2.json','analysis_params_extra_nodes_annot_3.json','analysis_params_extra_nodes_annot_4.json','analysis_params_extra_nodes_annot_5.json']"

mv master_summary.json master_summary_extra_nodes_annot.json

python ../../../analysis/average_over_simulations.py "['analysis_params_extra_nodes_no_annot_1.json','analysis_params_extra_nodes_no_annot_2.json','analysis_params_extra_nodes_no_annot_3.json','analysis_params_extra_nodes_no_annot_4.json','analysis_params_extra_nodes_no_annot_5.json']"

mv master_summary.json master_summary_extra_nodes_no_annot.json

