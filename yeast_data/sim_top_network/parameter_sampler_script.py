import json,os
import DSGRN
import numpy as np
import pattern_match_single_param as pmsp
import hillmodel
import pandas as pd
import datetime, time

# DSGRN yeast network specification
# # This network is one of the top scorers in the extra nodes no annotations results1 simulations
# network_spec = """
# ASH1 : (SWI5) : E
# HCM1 : (~NRM1)(~SWI5) : E
# NRM1 : (HCM1 + SWI4) : E
# SWI4 : (~WHI5) : E
# SWI5 : (NRM1) : E
# WHI5 : (~ASH1) : E
# """

# This network is one of the top scorers in the extra nodes no annotations results1 simulations
network_spec = """
ASH1 : (~SWI4) : E
HCM1 : (SWI4) : E
NRM1 : (HCM1) : E
SWI4 : (~NRM1)(~WHI5) : E
WHI5 : (~ASH1)(~NRM1) : E"""

# Specify results folder
results_dir = "results_20220726"

# Create the DSGRN network object
network = DSGRN.Network(network_spec)

# Compute the DSGRN parameter graph
param_graph = DSGRN.ParameterGraph(network)

# Find stable FCs
fc_params = []
for p in range(param_graph.size()):
    parameter = param_graph.parameter(p)
    dg = DSGRN.DomainGraph(parameter)
    mg = DSGRN.MorseGraph(dg)
    stable_FC_annotations = [mg.annotation(i)[0] for i in range(0, mg.poset().size())
                                if mg.annotation(i)[0].startswith("FC") and len(mg.poset().children(i)) == 0]
    if stable_FC_annotations:
        fc_params.append(p)
print("There are {} stable FC parameters out of {}.".format(len(fc_params),param_graph.size()))


# Create a DSGRN parameter node sampler object
sampler = DSGRN.ParameterSampler(network)

# Specify the ODE model Hill exponent, initial conditions, and time series sampling range and rate
hill_exp = 8
init_cond = np.random.rand(network.size())
trace_begin = 0
trace_end = 10
trace_num_samp = 50
fold_thresh = 3
# ---------------------------------------------


# Specify a parameter node in the parameter graph
for param_ind in fc_params:

    start_time = time.time()

    param_node = param_graph.parameter(param_ind)

    count = 0
    fold_change_count = 0

    while count < 1000:

        count = count + 1

        # Sample from the specified parameter node
        param_samp_dict = json.loads(sampler.sample(param_node))['Parameter']  # dictionary of parameter choices

        # Create Hill function ODE models
        hill_model = hillmodel.hillmodel(network_spec, param_samp_dict, hill_exp,old_format=False)

        # Integrate parameterized ODEs for long time to remove transients, using a fine time step
        # This helps identify fold changes within a stable full cycle, instead of within transients
        fine_time, fine_traces, nodes = hill_model.simulateHillModel(init_cond, 0, 20, .01)

        # Integrate parameterized ODEs for shorter time with coarser sampling
        samp_time, samp_traces, nodes = hill_model.simulateHillModel(fine_traces[-1],
                                                                     trace_begin,
                                                                     trace_end,
                                                                     (trace_end - trace_begin) / trace_num_samp)

        # Check that the fold change of all traces exceeds the threshold
        fold_change = all(
            (np.array(samp_traces).max(axis=0) - np.array(samp_traces).min(axis=0)) / np.array(samp_traces).min(
                axis=0) >= fold_thresh)

        if fold_change:
            fold_change_count += 1
            # plot the traces for visual inspection and sanity check
            hill_model.plotResults(samp_time, samp_traces,
                           savename=os.path.join(results_dir,"data_plot_p{}.pdf".format(param_ind)),
                           show=False,
                           plotoptions={'linewidth':2},
                           legendoptions={'fontsize':24,'loc':'upper left', 'bbox_to_anchor':(1, 1)},
                           figuresize = (20,10),
                           labeloptions={'xlabel' : 'Time', 'ylabel' : 'Expression','fontsize' : 14}
                           )
            # Check that the coarse time series at this Hill function parameter matches the dynamic predictions of DSGRN
            full_cycle_match, poset = pmsp.main_fc_only(param_ind, param_graph, network, samp_time, samp_traces, epsilons=[0.05])
            if full_cycle_match:
                break

    print("For parameter index {}, there were {} samples tested, match found = {}.".format(param_ind,count,fold_change and full_cycle_match))
    print("The number of samples with an acceptable fold change was {}.".format(fold_change_count))
    print("Time elapsed is {:02f} minutes.".format((time.time() - start_time)/60))

    if fold_change and full_cycle_match:
        # Save the parameter values and coarsely-sampled time series traces of the ODE simulatation to disk
        yeast_dict = {'Network': network.specification(),
                        'Parameter Index': param_ind,
                        'Parameter': param_samp_dict,
                        'Hill Exponent': hill_exp,
                        'Time': samp_time,
                        'Traces': [list(trace) for trace in samp_traces],
                        'Nodes': nodes,
                        'Poset at eps = 0.0' : [list(poset[0][1][0]),list(poset[0][1][1])]}

        with open(os.path.join(results_dir,'hillmodel_p{}.json'.format(param_ind)), 'w') as f:
            json.dump(yeast_dict, f)

        trace_df = pd.DataFrame()
        trace_df['time'] = samp_time
        for i in range(len(nodes)):
            trace_df[nodes[i]] = np.array(samp_traces)[:, i]
        trace_df = trace_df.set_index("time").transpose().rename_axis("gene_ID")       

        # Save the time series traces to a pipeline readable .tsv file
        with open(os.path.join(results_dir,'data_p{}.tsv'.format(param_ind)), 'w') as f:
            f.write("# Time series output to pipeline readable format\n")
            f.write("# Generated on %s\n" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            trace_df.to_csv(f, sep='\t', index=False)



