import pandas as pd
import sys, os

def transpose_file(data_tsv):
    df = pd.read_csv(data_tsv,sep="\t",comment="#")
    df = df.set_index("time").transpose().rename_axis("gene_ID")
    df.to_csv(data_tsv,sep="\t",index=True)

if __name__ == "__main__":
    transpose_file(sys.argv[1])